// Playtune bytestream for file "warning.mid" created by MIDITONES V1.9 on Wed Feb 19 18:33:22 2020
// command line: ./miditones warning
#ifdef __AVR__
#include <avr/pgmspace.h>
#else
#define PROGMEM
#endif
const unsigned char PROGMEM fail[] = {
    0x90, 52, 0x91, 64, 0x92, 59, 0, 100, 0x80, 0x81, 0x82, 0x90, 68, 0x91, 74, 0x92, 71, 0, 100, 0x81, 0x82,
    0, 100, 0x80, 0x90, 74, 0x91, 71, 0x92, 68, 0, 100, 0x80, 0x82, 0x90, 74, 0x92, 68, 0, 100, 0x80, 0x81,
    0x82, 0, 100, 0x90, 65, 0x91, 60, 0x92, 53, 0, 100, 0x80, 0x81, 0x82, 0x90, 75, 0x91, 72, 0x92, 69, 0, 100,
    0x80, 0x81, 0, 100, 0x82, 0x90, 75, 0x91, 72, 0x92, 69, 0, 100, 0x80, 0x82, 0x90, 75, 0x92, 69, 0, 100,
    0x80, 0x81, 0x82, 0, 100, 0x90, 66, 0x91, 61, 0x92, 54, 0, 100, 0x80, 0x81, 0x82, 0x90, 76, 0x91, 73, 0x92, 70,
    0, 100, 0x80, 0x81, 0, 100, 0x82, 0x90, 76, 0x91, 73, 0x92, 70, 0, 100, 0x80, 0x82, 0x90, 76, 0x92, 70,
    0, 100, 0x80, 0x81, 0x82, 0, 100, 0x90, 77, 0x91, 55, 0x92, 71, 0, 100, 0x80, 0x81, 0, 100, 0x82, 0x90, 77,
    0x91, 55, 0x92, 71, 2, 88, 0x80, 0x82, 0, 200, 0x81, 0xf0};
// This score contains 161 bytes, and 3 tone generators are used.
