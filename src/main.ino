
#include <Arduboy2.h>
#include <ArduboyPlaytune.h>
#include <EEPROM.h>
#include <HID-Project.h>
#include <math.h>

#include "correct.h"
#include "fail.h"

#define KONAMI_BACKDOOR true
//#define DEBUG

// Width of each charcter including inter-character space
#define CHAR_WIDTH 6 * 3

// Height of each charater
#define CHAR_HEIGHT 8 * 3

#define MAX_LIVES 10
#define MAX_PASSWORD_LEN 255

#define MAX_SERIAL_LEN MAX_PASSWORD_LEN + 3

#define CURRENT_LIVES_LOCATION EEPROM_STORAGE_SPACE_START + 0
#define SECRET_CODE_LOCATION EEPROM_STORAGE_SPACE_START + 1
#define PASSWORD_LENGTH_LOCATION EEPROM_STORAGE_SPACE_START + 5
#define PASSWORD_LOCATION EEPROM_STORAGE_SPACE_START + 6

#define C5 beep.freq(523.251)
#define D5 beep.freq(587.330)
#define E5 beep.freq(659.255)
#define F5 beep.freq(698.456)

// Color array index
enum class Color {
  RED,
  GREEN,
  BLUE,
  COUNT
};

// Map LED color index to LED name
const byte LEDpin[(byte)(Color::COUNT)] = {RED_LED, GREEN_LED, BLUE_LED};

byte ledState[3] = {RGB_OFF, RGB_OFF, RGB_OFF};

Arduboy2 arduboy;
ArduboyPlaytune tunes(arduboy.audio.enabled);
BeepPin1 beep; // Create a class instance for speaker pin 1

const unsigned int FRAME_RATE = 15; // Frame rate in frames per second
size_t previousMillis = 0;
size_t interval = 500;

uint8_t currentCode[4] = {0};
uint8_t activeIndex = 0;

uint8_t konamiCodeState = 0;

// Loaded from EEPROM
uint8_t lives;
uint8_t secretCode[4] = {0};
char *password = NULL;

void printArray(byte output[], size_t len) {
  for (size_t i = 0; i < len; i++) {
    if (output[i] < 0x10) {
      Serial.print("0");
    }
    Serial.print(output[i], HEX);
    Serial.print(" ");
  }
  Serial.println();
}

void printSerialHelp() {
  Serial.println("Help:");

  Serial.print("Current lives: ");
  Serial.print(lives);
  Serial.println();

  Serial.print("Current code: ");
  printArray(currentCode, 4);
  Serial.println();

  Serial.println("------------------");
  Serial.println("To update secret code:");
  Serial.println("=>S=<4 ascii digits>");
  Serial.println("To update password:");
  Serial.println("=>P=<a string of your choosing>");
  Serial.println("Updating password resets lives to MAX_LIVES");
  Serial.println("WARNING: Updating secret code clears password");
  Serial.println("------------------");
  Serial.println();
}

bool clearPassword() {
  uint8_t passwordLen = EEPROM.read(PASSWORD_LENGTH_LOCATION);

  for (size_t i = 0; i < passwordLen; i++) {
    EEPROM.update(PASSWORD_LOCATION + i, '\0');
  }
  EEPROM.update(PASSWORD_LENGTH_LOCATION, 0);

  free(password);
  password = NULL;

#ifdef DEBUG
  Serial.println("Password cleared");
#endif

  return true;
}

// Input is 4 ascii characters
void updateSecretCode(char *newCode) {
  // Clear password
  clearPassword();

  // Update secret code
  for (size_t i = 0; i < sizeof(secretCode); i++) {
    uint8_t val = newCode[i] - '0';
    if (val >= 0 && val < 10) {
      secretCode[i] = val;
      EEPROM.update(SECRET_CODE_LOCATION + i, val);
    } else {
      secretCode[i] = 0;
      EEPROM.update(SECRET_CODE_LOCATION + i, 0);
    }
  }

#ifdef DEBUG
  printArray(secretCode, sizeof(secretCode));
#endif
}

boolean updatePassword(char *newPassword) {
  size_t passwordLen = strnlen(newPassword, MAX_PASSWORD_LEN);
  if (passwordLen >= MAX_PASSWORD_LEN) {
    return false;
  }

  clearPassword();
  password = (char *)malloc(passwordLen + 1);

  EEPROM.update(PASSWORD_LENGTH_LOCATION, passwordLen);
  for (size_t i = 0; i < passwordLen; i++) {
    EEPROM.update(PASSWORD_LOCATION + i, newPassword[i]);
    password[i] = newPassword[i];
  }
  password[passwordLen] = '\0';

  EEPROM.update(CURRENT_LIVES_LOCATION, MAX_LIVES);
  lives = MAX_LIVES;

#ifdef DEBUG
  Serial.println(newPassword);
#endif

  return true;
}

// Technically I could just pull the serial data from serialBuffer
void parseSerialInput(char *input) {
  size_t len = strnlen(input, MAX_SERIAL_LEN);


#ifdef DEBUG
  Serial.print("Received input[");
  Serial.print(len);
  Serial.print("]: ");
  Serial.print(input);
  Serial.println();
#endif

  if (len == 0 || len >= MAX_SERIAL_LEN) {
    printSerialHelp();
    return;
  }

  if (strncmp(input, "help", 4) == 0) {
    printSerialHelp();
    return;
  }

  switch(input[0]) {
    case 'S': // S=XXXX\0
      if (len != 7) {
        Serial.println("Incorrect 'S' length");
        printSerialHelp();
        return;
      }
      updateSecretCode(input+2);
      break;
    case 'P':
      updatePassword(input+2);
      break;
    default:
      Serial.println("Unknown input");
      printSerialHelp();
      break;
  }
}

void resetCurrentCode() {
  for (size_t i = 0; i < sizeof(currentCode); i++) {
    currentCode[i] = 0;
  }
  activeIndex = 0;
}

void success() {
  ledState[(byte)(Color::GREEN)] = RGB_ON;
  if (!tunes.playing()) {
    tunes.playScore(correct);
  }
  if (password) {
    Keyboard.print(password);
  }
  EEPROM.update(CURRENT_LIVES_LOCATION, MAX_LIVES);
  lives = MAX_LIVES;
  resetCurrentCode();
}

void wrong() {
  ledState[(byte)(Color::RED)] = RGB_ON;
  tunes.playScore(fail);
  if (lives > 0) {
    lives--;
    EEPROM.update(CURRENT_LIVES_LOCATION, lives);
  }
}

void printCurrentCode() {
  for (size_t i = 0; i < sizeof(currentCode); i++) {
    boolean active = (i == activeIndex);

    int16_t y = (HEIGHT / 3);
    int16_t x = (CHAR_WIDTH * 1.5 * i) + CHAR_WIDTH;
    uint8_t size = 3;

    arduboy.drawChar(x, y, (char)('0' + currentCode[i]), WHITE, BLACK, size);

    //Decorative arrows
    size_t padding = 3;
    size_t above = y - padding;
    size_t below = y + CHAR_HEIGHT;
    size_t baseWidth = CHAR_WIDTH - padding;

    if (active) {
      arduboy.fillTriangle(x, above, x + baseWidth, above, x + baseWidth / 2, y - CHAR_WIDTH);
      arduboy.fillTriangle(x, below, x + baseWidth, below, x + baseWidth / 2, below + CHAR_WIDTH - padding);
    }
  }
}

void populateEERPM() {
  // Populate initial values
  EEPROM.update(CURRENT_LIVES_LOCATION, MAX_LIVES);

  EEPROM.update(SECRET_CODE_LOCATION + 1, 0);
  EEPROM.update(SECRET_CODE_LOCATION + 2, 1);
  EEPROM.update(SECRET_CODE_LOCATION + 3, 2);
  EEPROM.update(SECRET_CODE_LOCATION + 4, 3);

  EEPROM.update(PASSWORD_LENGTH_LOCATION, 8);
  EEPROM.update(PASSWORD_LOCATION + 0, 'p');
  EEPROM.update(PASSWORD_LOCATION + 1, 'a');
  EEPROM.update(PASSWORD_LOCATION + 2, 's');
  EEPROM.update(PASSWORD_LOCATION + 3, 's');
  EEPROM.update(PASSWORD_LOCATION + 4, 'w');
  EEPROM.update(PASSWORD_LOCATION + 5, 'o');
  EEPROM.update(PASSWORD_LOCATION + 6, 'r');
  EEPROM.update(PASSWORD_LOCATION + 7, 'd');
}

void setup() {
  arduboy.begin();

  //arduboy.writeShowBootLogoFlag(false);
  //arduboy.writeShowBootLogoLEDsFlag(false);

  arduboy.setFrameRate(FRAME_RATE);
  arduboy.setTextWrap(false);
  arduboy.setTextSize(3);
  arduboy.clear();
  arduboy.setCursor(0, 0);

  beep.begin(); // Set up the hardware for playing tones

  Keyboard.begin();
  Serial.begin(19200);

  tunes.initChannel(PIN_SPEAKER_1);
  tunes.initChannel(PIN_SPEAKER_2);

  lives = EEPROM.read(CURRENT_LIVES_LOCATION);

  // Load secret code
  for (size_t i = 0; i < sizeof(secretCode); i++) {
    uint8_t val = EEPROM.read(SECRET_CODE_LOCATION + i);
    if (val >= 0 && val < 10) {
      secretCode[i] = val;
    } else {
      secretCode[i] = 0;
    }
  }

  // load password
  uint8_t passwordLen = EEPROM.read(PASSWORD_LENGTH_LOCATION);
  password = (char *)malloc(passwordLen + 1);
  if (password) {
    for (size_t i = 0; i < passwordLen; i++) {
      password[i] = EEPROM.read(PASSWORD_LOCATION + i);
    }
    password[passwordLen] = '\0';
  }
}

void digitalSet() {
  arduboy.digitalWriteRGB(ledState[(byte)(Color::RED)], ledState[(byte)(Color::GREEN)], ledState[(byte)(Color::BLUE)]);
}

char serialBuffer[MAX_SERIAL_LEN] = {0};
size_t serialIndex = 0;

void loop() {
  size_t currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;

    ledState[(byte)(Color::RED)] = RGB_OFF;
    ledState[(byte)(Color::GREEN)] = RGB_OFF;
    ledState[(byte)(Color::BLUE)] = RGB_OFF;
  }

  if (Serial.available() && serialIndex < MAX_SERIAL_LEN) {
    char nextChar = Serial.read();
    if (nextChar == '\n') {
      serialBuffer[serialIndex] = '\0';
      serialIndex = 0;
      parseSerialInput(serialBuffer);
    } else {
      serialBuffer[serialIndex++] = nextChar;
    }
  }

  // pause render until it's time for the next frame
  if (!(arduboy.nextFrame())) {
    return;
  }

  // Lockout
  if (lives == 0) {
    arduboy.clear();
    arduboy.drawLine(0, 0, WIDTH, HEIGHT);
    arduboy.drawLine(0, HEIGHT, WIDTH, 0);
    arduboy.display();
    return;
  }

  // The timer() function is called once per frame, so duration values will be
  // the number of frames that the tone plays for.
  // At 25 frames per second each frame will be 40ms.
  beep.timer(); // handle tone duration

  arduboy.pollButtons();
  if (arduboy.justPressed(UP_BUTTON)) {
    currentCode[activeIndex] = (currentCode[activeIndex] + 1) % 10;
    beep.tone(D5, 1);

    if (konamiCodeState == 0 || konamiCodeState == 1) {
      konamiCodeState++;
    } else {
      konamiCodeState = 0;
    }
  }

  if (arduboy.justPressed(RIGHT_BUTTON)) {
    activeIndex = (activeIndex + 1) % sizeof(currentCode);

    if (konamiCodeState == 5 || konamiCodeState == 7) {
      konamiCodeState++;
    } else {
      konamiCodeState = 0;
    }
  }

  if (arduboy.justPressed(DOWN_BUTTON)) {
    currentCode[activeIndex] = ((currentCode[activeIndex] - 1) + 10) % 10;
    beep.tone(C5, 1);

    if (konamiCodeState == 2 || konamiCodeState == 3) {
      konamiCodeState++;
    } else {
      konamiCodeState = 0;
    }
  }

  if (arduboy.justPressed(LEFT_BUTTON)) {
    activeIndex = ((activeIndex - 1) + sizeof(currentCode)) % sizeof(currentCode);

    if (konamiCodeState == 4 || konamiCodeState == 6) {
      konamiCodeState++;
    } else {
      konamiCodeState = 0;
    }
  }

  // B button is on the right
  if (arduboy.justPressed(B_BUTTON)) {
    // TODO: Check lives before executing `success`. (Exclude konami from check?)

    if (konamiCodeState == 9) {
      if (KONAMI_BACKDOOR) {
        success();
        konamiCodeState = 0;
        return;
      } else {
        wrong();
        return;
      }
    }

    for (size_t i = 0; i < sizeof(currentCode); i++) {
      if (currentCode[i] != secretCode[i]) {
        wrong();
        return;
      }
    }
    success();
  }

  // A button is on the left
  if (arduboy.justPressed(A_BUTTON)) {
    ledState[(byte)(Color::BLUE)] = RGB_ON;
    resetCurrentCode();

    if (konamiCodeState == 8) {
      konamiCodeState++;
    } else {
      konamiCodeState = 0;
    }
  }

  arduboy.clear();

  if (lives == MAX_LIVES) {
    arduboy.drawFastHLine(0, 0, WIDTH);
  } else {
    arduboy.drawFastHLine(0, 0, WIDTH / MAX_LIVES * lives);
  }

  printCurrentCode();
  arduboy.display();
  digitalSet();
}
