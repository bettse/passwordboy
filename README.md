# Passwordboy

Combination lock UI on an arduboy that outputs password as keyboard input for correct combination

## Controls

- Left/Right to select a barrel
- Up/Down to increment/decrement value
- A (left red button) to reset to `0000`
- B (right red button) to 'submit'
  - Incorrect combination will flash red LED briefly
  - Correct combination will flash green LED friendly, and send password as keyboard input

![Device](./device.jpeg "Picture of device showing combination '1 2 3 4'")
